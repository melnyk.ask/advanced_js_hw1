// 1.Прототипне наслідування - це можливість використовувати властивості і методи одного об'єкту при створенні іншого об'єкту. Тобто ми не
// створюємо властивості, методи, а беремо їх в іншого (батьківського) об'єкту.
// 2. Super() використовується в середині методу consntructor для того, щоб викликати конструктор батьківського класу.
// Таким чином є можливість в класі-нащадку використовувати (успадкувати) властивості і методи батьківського класу.

class Employee { 
    constructor(name, age, salary) { 
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() { 
        return this._name;
    }

    set name(value) { 
        this._name = value;
    }

    get age() { 
        return this._age;
    }

    set age(value) { 
        this._age = value;
    }

    get salary() { 
        return this._salary;
    }

    set salary(value) { 
        this._salary = value;
    }
}

class Programmer extends Employee { 
    constructor(name, age, salary, lang) { 
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() { 
        return +this._salary*3;
    }
}

let prg1 = new Programmer("Stepan", 23, 1000, "english");
let prg2 = new Programmer("Konrad", 25, 2000, "spanish");
let prg3 = new Programmer("Goga", 55, 4000, "portugal");


console.log(prg1, prg2, prg3);